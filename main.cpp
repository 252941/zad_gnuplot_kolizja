#include <iostream>
#include <iomanip>
#include "lacze_do_gnuplota.hh"
#include "Dron.hh"
#include "Wektor3D.hh"

using namespace std;
int main()
{
  PzG::LaczeDoGNUPlota  Lacze;
  Dron dron;
  Dron wirnik1;
        wirnik1.wierzcholek[0] = Wektor3D(25,40,-65);
        wirnik1.wierzcholek[1] = Wektor3D(25,40,-55);
        wirnik1.wierzcholek[2] = Wektor3D(35,40,-55);
        wirnik1.wierzcholek[3] = Wektor3D(35,40,-65);

        wirnik1.wierzcholek[4] = Wektor3D(35,60,-65);
        wirnik1.wierzcholek[5] = Wektor3D(35,60,-55);
        wirnik1.wierzcholek[6] = Wektor3D(25,60,-55);
        wirnik1.wierzcholek[7] = Wektor3D(25,60,-65);
        wirnik1.ZapiszDoPlikuW1();
  Dron wirnik2;
        wirnik2.wierzcholek[0] = Wektor3D(45,40,-65);
        wirnik2.wierzcholek[1] = Wektor3D(45,40,-55);
        wirnik2.wierzcholek[2] = Wektor3D(55,40,-55);
        wirnik2.wierzcholek[3] = Wektor3D(55,40,-65);

        wirnik2.wierzcholek[4] = Wektor3D(55,60,-65);
        wirnik2.wierzcholek[5] = Wektor3D(55,60,-55);
        wirnik2.wierzcholek[6] = Wektor3D(45,60,-55);
        wirnik2.wierzcholek[7] = Wektor3D(45,60,-65);
        wirnik2.ZapiszDoPlikuW2();
  char c;
  Lacze.DodajNazwePliku("dat/dron.pow");
  Lacze.DodajNazwePliku("dat/powierzchnia1.pow");
  Lacze.DodajNazwePliku("dat/powierzchnia2.pow");
  Lacze.DodajNazwePliku("dat/wirnik1.pow");
  Lacze.DodajNazwePliku("dat/wirnik2.pow");
  Lacze.DodajNazwePliku("dat/przeszkoda1.pow");
  Lacze.DodajNazwePliku("dat/przeszkoda2.pow");
  Lacze.ZmienTrybRys(PzG::TR_3D);
  Lacze.Inicjalizuj();  // Tutaj startuje gnuplot.
  Lacze.UstawZakresX(0, 100);
  Lacze.UstawZakresY(0, 100);
  Lacze.UstawZakresZ(-100, 0);
  Lacze.UstawRotacjeXZ(40,60); // Tutaj ustawiany jest widok
  Lacze.Rysuj();
  for(;;)
  {
    cin >> noskipws >> c;
    dron.normalny();
    char wybor;
    cout<<"r - zadaj ruch na wprost"<< endl <<  "y - obroc drona na boki" << endl << "x - obroc drona w gore/dol" << endl << "k - koniec dzialania programu" << endl;
    cin>>wybor;
    if(wybor== 'k') break;
    if(wybor == 'r')
    {
      int odleglosc;
      cout<<"Podaj odleglosc: ";
      cin.ignore();
      cin.clear();
      cin>>odleglosc;
      dron.PrzesunWprost(-odleglosc);
      wirnik1.PrzesunWprost(-odleglosc);
      wirnik2.PrzesunWprost(-odleglosc);
    }
    if(wybor == 'x')
    {
      float katx;
      cout<<"Podaj kat: "<<endl;
      cin.ignore();
      cin.clear();
      cin>>katx;
      dron.UstawKatOpadania(katx);
      wirnik1.UstawKatOpadania(katx);
      wirnik2.UstawKatOpadania(katx);
    }
    if(wybor == 'y')
    {
      float katy;
      cout<<"Podaj kat: "<<endl;
      cin.ignore();
      cin.clear();
      cin>>katy;
      dron.UstawKatObrotu(katy);
      wirnik1.UstawKatObrotu(katy);
      wirnik2.UstawKatObrotu(katy);
    }

    dron.ZapiszDoPlikuD();
    wirnik1.ZapiszDoPlikuW1();
    wirnik2.ZapiszDoPlikuW2();
    Lacze.UsunWszystkieNazwyPlikow();
    Lacze.DodajNazwePliku("dat/dron.pow");
    Lacze.DodajNazwePliku("dat/wirnik1.pow");
    Lacze.DodajNazwePliku("dat/wirnik2.pow");
    Lacze.DodajNazwePliku("dat/powierzchnia1.pow");
    Lacze.DodajNazwePliku("dat/powierzchnia2.pow");
    Lacze.DodajNazwePliku("dat/przeszkoda1.pow");
    Lacze.DodajNazwePliku("dat/przeszkoda2.pow");
    Lacze.Rysuj();
  }
};
