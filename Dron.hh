#ifndef MACIERZ_HH
#define MACIERZ_HH

#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include "lacze_do_gnuplota.hh"
#include "Wektor3D.hh"

class Dron
{
    public:
    Wektor3D wierzcholek[8];
    Wektor3D normalny();// wskazuje przód
    Dron();
    void PrzesunWprost(double odleglosc); // dodajemy do kazdego wierzcholka normalny/normalny.Norma() * dlugosc;
    void UstawKatOpadania(float kat);
    void UstawKatObrotu(float kat);
    void ZapiszDoPlikuD();
    void ZapiszDoPlikuW1();
    void ZapiszDoPlikuW2();
    int CzyKolizja();
};

#endif
