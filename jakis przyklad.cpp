#include <iostream>
#include <iomanip>
#include "lacze_do_gnuplota.hh"
using namespace std;
int main()
{
  PzG::LaczeDoGNUPlota  Lacze;
  char c;
  Lacze.DodajNazwePliku("dat/prostopadloscian1.pow");
  Lacze.ZmienTrybRys(PzG::TR_3D);
  Lacze.Inicjalizuj();  // Tutaj startuje gnuplot.
  Lacze.UstawZakresX(-40, 100);
  Lacze.UstawZakresY(-90, 90);
  Lacze.UstawZakresZ(-20, 90);
  Lacze.UstawRotacjeXZ(40,60); // Tutaj ustawiany jest widok
  Lacze.Rysuj();        // Teraz powinno pojawic sie okienko gnuplota
                        // z rysunkiem, o ile istnieje plik "prostopadloscian1.pow"
  cout << "Nacisnij ENTER, aby zobaczyc prostopadloscian nr 2 ... " << flush;
  cin >> noskipws >> c;
  Lacze.UsunWszystkieNazwyPlikow();
  Lacze.DodajNazwePliku("dat/prostopadloscian2.pow");
  Lacze.Rysuj();        // Teraz powinno pojawic sie okienko gnuplota
                        // z rysunkiem, o ile istnieje plik "prostopadloscian2.pow"
  cout << "Nacisnij ENTER, aby zobaczyc prostopadloscian nr 3 ... " << flush;
    cin >> noskipws >> c;
  Lacze.UsunWszystkieNazwyPlikow();
  Lacze.DodajNazwePliku("dat/prostopadloscian3.pow");
  Lacze.Rysuj();        // Teraz powinno pojawic sie okienko gnuplota
                        // z rysunkiem, o ile istnieje plik "prostopadloscian3.pow"
  cout << "Nacisnij ENTER, aby zakonczyc ... " << flush;
  cin >> noskipws >> c;
}

/*
#include <iostream>
#include <iomanip>
#include <fstream>
#include "lacze_do_gnuplota.hh"
using namespace std;
#define PLIK_SCIEZKI   "dat/sciezka.xy"
#define PLIK_ROBOTA    "dat/robot.xyr"
#define PLIK_STARTU    "dat/start.xyr"
#define ROZM_STARTU    3
#define ROZM_ROBOTA    5
#define X0     10
#define Y0     10
void ZainicjalizujStanPoczatkowy()
{
  ofstream  StrmWy;
  StrmWy.open(PLIK_STARTU);
  StrmWy << X0 << " " << Y0 << " " << ROZM_STARTU << endl;
  StrmWy.close();
  StrmWy.open(PLIK_SCIEZKI);
  StrmWy << X0 << " " << Y0 << endl;
  StrmWy.close();
  StrmWy.open(PLIK_ROBOTA);
  StrmWy << X0 << " " << Y0 << " " << ROZM_ROBOTA << endl;
  StrmWy.close();
}
#define X1    25
#define Y1    12
void UtworzSciezke1()
{
  ofstream  StrmWy;
  StrmWy.open(PLIK_SCIEZKI,ios::app);
  StrmWy << X1 << " " << Y1 << endl;
  StrmWy.close();
  StrmWy.open(PLIK_ROBOTA);
  StrmWy << X1 << " " << Y1 << " " << ROZM_ROBOTA << endl;
  StrmWy.close();
}
#define X2    37
#define Y2    25
void UtworzSciezke2()
{
  ofstream  StrmWy;
  StrmWy.open(PLIK_SCIEZKI,ios::app);
  StrmWy << X2 << " " << Y2 << endl;
  StrmWy.close();
  StrmWy.open(PLIK_ROBOTA);
  StrmWy << X2 << " " << Y2 << " " << ROZM_ROBOTA << endl;
  StrmWy.close();
}
int main2()
{
  char  Opcja;
  PzG::LaczeDoGNUPlota  Lacze;
  Lacze.DodajNazwePliku_PunktyRoznejWielkosci("dat/przeszkody.xyr");
  Lacze.DodajNazwePliku_PunktyRoznejWielkosci("dat/start.xyr").
      ZmienEtykiete("Start");
  Lacze.DodajNazwePliku_PunktyRoznejWielkosci("dat/cel.xyr").
      ZmienEtykiete("Cel");
  Lacze.DodajNazwePliku_PunktyRoznejWielkosci("dat/robot.xyr").
      ZmienEtykiete("Robot");
  Lacze.DodajNazwePliku_Lamana("dat/sciezka.xy").
      ZmienStyl(5).ZmienSzerokosc(2);
  Lacze.Inicjalizuj();  // Tutaj startuje gnuplot.
  Lacze.ZmienTrybRys(PzG::TR_2D);
  Lacze.UstawZakresY(0,80);
  Lacze.UstawZakresX(0,100);
  cin >> noskipws;
  ZainicjalizujStanPoczatkowy();
  Lacze.Rysuj();
  cout << "Krok 1: Start. Aby kontynuowac nacisnij ENTER ..." << flush;
  cin >> Opcja;
  UtworzSciezke1();
  Lacze.Rysuj();
  cout << "Krok 2: Idziemy do przodu!!! Aby kontynuowac nacisnij ENTER ..." << flush;

  cin >> Opcja;
  UtworzSciezke2();
  Lacze.Rysuj();
  cout << "Krok 3: I tu niestety koniec :( Aby zakonczyc nacisnij ENTER ..." << flush;
  cin >> Opcja;
}
*/
